<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\MainVisual;
use App\Models\Shop;
use Illuminate\Http\Request;

class TopController extends Controller
{
    public function index()
    {
        $PriorityShops = Shop::priorityShop()->orderBy('shop_rank', 'desc')->get();
        $FirstAppearanceShops = Shop::firstAppearanceShop()->inRandomOrder()->get();
        $NonStatusShops = Shop::nonStatusShop()->inRandomOrder()->get();
        $shops = [];

        foreach ($NonStatusShops as $NonStatusShop) {
            array_push($shops, $NonStatusShop);
        }

        foreach ($FirstAppearanceShops as $FirstAppearanceShop) {
            array_push($shops, $FirstAppearanceShop);
        }

        foreach ($PriorityShops as $PriorityShop) {
            array_unshift($shops, $PriorityShop);
        }

        $MainVisual = MainVisual::orderBy('id', 'desc')->first();
        $Information = Information::orderBy('id', 'desc')->first();

        return view('top')->with([
            'shops' => $shops,
            'MainVisual' => $MainVisual,
            'Information' => $Information,
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN_HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    public function showLogoutForm()
    {
        return view('auth.logout');
    }
    public function showLogoutComlete()
    {
        return view('auth.logout_complete');
    }


    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        return $this->loggedOut($request);

    }

    public function loggedOut(Request $request)
    {
        return redirect(route('admin.login'));
    }

    public function selectLoginForm()
    {
        return view('auth.login_select');
    }

    public function redirectPath()
    {
        return 'admin/shop';
    }
}

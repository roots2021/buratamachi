<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Information;

class InfoController extends Controller
{
    public function index() {
        $Information = Information::first();
        return view('admin.info.list')->with([
            'Information' => $Information
        ]);
    }

    public function edit(Request $request) {
        $Information = Information::first();
        return view('admin.info.edit')->with([
            'Information' => $Information
        ]);
    }

    public function editPost(Request $request) {
        $Information = Information::first();
        $data = $request->all();

        if (empty($Information)) {
            Information::create($data);
        } else {
            $Information->fill($data)->save();
        }

        return redirect()->route('admin.info');
    }
}

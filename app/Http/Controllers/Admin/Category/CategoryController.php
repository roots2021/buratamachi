<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $Categories = Category::all();

        return view('admin.category.list')->with([
            'Categories' => $Categories,
        ]);
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function createPost(Request $request)
    {
        Category::create([
            'category_name' => $request->category_name
        ]);

        return redirect()->route('admin.category');
    }

    public function edit(Request $request) {
        $Category = Category::find($request->category_id);

        return view('admin.category.edit')->with([
            'Category' => $Category,
        ]);
    }

    public function update(Request $request) {
        $Category = Category::find($request->category_id);

        $data = $request->all();
        $Category->fill($data)->save();

        return redirect()->route('admin.category');
    }

    public function delete(Request $request)
    {
        $Category = Category::find($request->category_id);
        $Category->delete();

        return redirect()->route('admin.category');
    }
}

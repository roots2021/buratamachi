<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ShopController extends Controller
{
    public function index() {
        $Shops = Shop::all();
        return view('admin.shop.list')->with([
            'Shops' => $Shops
        ]);
    }

    public function detail(Request $request) {
        $Shop = Shop::find($request->shop_id);
        $shopThumbnail = storage_path('public/thumbnail/1_thumbnail.png');

        return view('admin.shop.detail')->with([
            'Shop' => $Shop,
            'shopThumbnail' => $shopThumbnail
        ]);
    }

    public function create()
    {
        $Categories = Category::get();

        return view('admin.shop.create')->with([
            'Categories' => $Categories,
        ]);
    }

    public function store(Request $request)
    {
        $image = $request->file('thumbnail_url');
        $path = '';
        if (isset($image)) {
            $path = env('APP_URL') . '/storage/' . $image->storeAs('shop', $image->getClientOriginalName(), 'public');
        }

        $data = $request->all();
        $data['thumbnail_url'] = $path;
        $Shop = Shop::create($data);
        $Shop->save();

        return redirect()->route('admin.shop');
    }

    public function edit(Request $request) {
        $Categories = Category::get();
        $Shop = Shop::find($request->shop_id);

        return view('admin.shop.edit')->with([
            'Shop' => $Shop,
            'Categories' => $Categories,
        ]);
    }

    public function editPost(Request $request) {
        $Shop = Shop::find($request->shop_id);
        $image = $request->file('thumbnail_url');

        $path = '';
        if (isset($image)) {
            $path = env('APP_URL') . '/storage/' . $image->storeAs('shop', $image->getClientOriginalName(), 'public');
        } else {
            $path = $Shop['thumbnail_url'];
        }

        $data = $request->all();
        $data['thumbnail_url'] = $path;

        $Shop->fill($data)->save();

        return view('admin.shop.detail')->with([
            'Shop' => $Shop
        ]);
    }

    public function delete(Request $request) {
        $Shop = Shop::find($request->shop_id);
        $Shop->delete();

        return redirect()->route('admin.shop');
    }
}

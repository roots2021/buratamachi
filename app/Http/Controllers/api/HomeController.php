<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Information;
use App\Models\MainVisual;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function mainVisual()
    {
        $AllMainVisual = MainVisual::all();
        $displayMainVisual = [];

        // 最新の1つを取りたいだけだが、全件取得をしてしまっている。
        foreach ($AllMainVisual as $index => $mainVisual) {
            if ($index === (count($AllMainVisual) - 1)) {
                array_push($displayMainVisual, $mainVisual);
            }
        }

        return $displayMainVisual;
    }

    public function information()
    {
        $AllInformation = Information::all();
        $displayInformation = [];

        // メインビジュアル同様、リファクタリング要素あり。
        foreach ($AllInformation as $index => $information) {
            if ($index === (count($AllInformation) - 1)) {
                array_push($displayInformation, $information);
            }
        }

        return $displayInformation;
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Shop;

class ShopController extends Controller
{
    public function index()
    {
        $Shops = Shop::all();
        $displayShop = [];

        foreach ($Shops as $shop) {
            $CategoryName = Category::find($shop['category_id'])['category_name'];
            $shop['category_name'] = $CategoryName;
            array_push($displayShop, $shop);
        }

        return response()->json($displayShop, 200);
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'introduction',
        'sub_introduction',
        'shop_name',
        'shop_rank',
        'shop_status',
        'shop_url',
        'thumbnail_url',
    ];

    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function scopePriorityShop(Builder $query)
    {
        return $query->where($this->table . '.shop_status', '1');
    }

    public function scopeFirstAppearanceShop(Builder $query)
    {
        return $query->where($this->table . '.shop_status', '0');
    }

    public function scopeNonStatusShop(Builder $query)
    {
        return $query->where($this->table . '.shop_status', 2);
    }
}

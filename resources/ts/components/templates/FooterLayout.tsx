import { memo, VFC } from 'react';
import { Footer } from '../organisms/layout/Footer';

export const FooterLayout: VFC = memo(() => {
    return <Footer />;
});

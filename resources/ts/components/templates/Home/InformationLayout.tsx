import { WrapItem } from '@chakra-ui/react';
import { memo, useEffect, VFC } from 'react';
import { useInformation } from '../../../hooks/useInformation';
import { Information } from '../../organisms/Information';

export const InformationLayout: VFC = memo(() => {
    const { getInformation, information } = useInformation();

    useEffect(() => getInformation(), []);

    return (
        <>
            {information.map((shop) => (
                <WrapItem key={shop.id} mx="auto" justifyContent="center">
                    <Information content={shop.content} />
                </WrapItem>
            ))}
        </>
    );
});

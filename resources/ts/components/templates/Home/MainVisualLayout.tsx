import { WrapItem } from '@chakra-ui/react';
import { memo, useEffect, VFC } from 'react';
import { useMainVisual } from '../../../hooks/useMainVisual';
import { MainVisual } from '../../organisms/MainVisual';

export const MainVisualLayout: VFC = memo(() => {
    const { getMainVisual, mainVisual } = useMainVisual();

    useEffect(() => getMainVisual(), []);

    const sortedMainVisual = mainVisual.slice();

    sortedMainVisual.sort(function (a, b) {
        if (a.id < b.id) {
            return 1;
        } else {
            return -1;
        }
    });

    return (
        <>
            {sortedMainVisual.map((mainVisual) => (
                <WrapItem key={mainVisual.id} mx="auto">
                    <MainVisual key={mainVisual.id} imageUrl={mainVisual.image_url} />
                </WrapItem>
            ))}
        </>
    );
});

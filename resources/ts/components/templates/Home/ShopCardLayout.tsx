/* eslint-disable react-hooks/exhaustive-deps */

import { Center, Spinner, useDisclosure, Wrap, WrapItem } from '@chakra-ui/react';
import { memo, useCallback, useEffect, VFC } from 'react';

import { ShopCard } from '../../organisms/shop/ShopCard';
import { useAllShops } from '../../../hooks/useAllShops';
import { useSelectShop } from '../../../hooks/useSelectShop';
import { UserDetailModal } from '../../organisms/shop/ShopCardDetailModal';

export const ShopCardLayout: VFC = memo(() => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const { getShops, shops, loading } = useAllShops();
    const { onSelectShop, selectedShop } = useSelectShop();

    useEffect(() => getShops(), []);

    const onClickShop = useCallback(
        (id: number) => {
            onSelectShop({ id, shops, onOpen });
        },
        [shops, onSelectShop, onOpen]
    );

    const sortedShops = shops.slice();

    for (var i = sortedShops.length; 1 < i; i--) {
        var k = Math.floor(Math.random() * i);
        [sortedShops[k], sortedShops[i - 1]] = [sortedShops[i - 1], sortedShops[k]];
    }

    sortedShops.sort(function (a, b) {
        if (a.shop_status !== b.shop_status) {
            if (a.shop_status < b.shop_status) {
                return 1;
            } else {
                return -1;
            }
        }

        if (a.shop_rank > b.shop_rank) {
            return 1;
        } else {
            return -1;
        }
    });

    return (
        <>
            {loading ? (
                <Center h="100vh">
                    <Spinner />
                </Center>
            ) : (
                <Wrap p={{ base: 4, md: 10 }}>
                    {sortedShops.map((shop) => (
                        <WrapItem key={shop.id} mx="auto">
                            <ShopCard
                                id={shop.id}
                                thumbnailUrl={shop.thumbnail_url}
                                shopName={shop.shop_name}
                                shopStatus={shop.shop_status}
                                categoryName={shop.category_name}
                                introduction={shop.introduction}
                                onClick={onClickShop}
                            />
                        </WrapItem>
                    ))}
                </Wrap>
            )}
            <UserDetailModal shop={selectedShop} isOpen={isOpen} onClose={onClose} />
        </>
    );
});

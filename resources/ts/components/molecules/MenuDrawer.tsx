import { Button, Drawer, DrawerBody, DrawerContent, DrawerOverlay } from '@chakra-ui/react';
import { memo, VFC } from 'react';

type Props = {
    onClose: () => void;
    isOpen: boolean;
    onClickHome: () => void;
};

export const MenuDrawer: VFC<Props> = memo((props) => {
    const { isOpen, onClose, onClickHome } = props;
    return (
        <Drawer placement="left" size="sm" onClose={onClose} isOpen={isOpen}>
            <DrawerOverlay>
                <DrawerContent>
                    <DrawerBody p={0} bg="gray.100">
                        <Button w="100%" onClick={onClickHome}>
                            TOP
                        </Button>
                    </DrawerBody>
                </DrawerContent>
            </DrawerOverlay>
        </Drawer>
    );
});

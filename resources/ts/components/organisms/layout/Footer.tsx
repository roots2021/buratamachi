/* eslint-disable react-hooks/exhaustive-deps */

import { Box, Flex, Heading, Link, useDisclosure } from '@chakra-ui/react';
import { memo, useCallback, VFC } from 'react';
import { useNavigate } from 'react-router-dom';
import { MenuIconButton } from '../../atoms/button/MenuIconButton';
import { MenuDrawer } from '../../molecules/MenuDrawer';

export const Footer: VFC = memo(() => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const navigate = useNavigate();

    const onClickHome = useCallback(() => navigate('/'), []);

    return (
        <>
            <Flex
                as="nav"
                bg="blue.700"
                color="white"
                align="center"
                justify="center"
                mt={10}
                padding={{ base: 3, md: 5 }}
            >
                <Flex align="center">
                    <Heading as="h1" fontSize={{ base: 'lg', md: 'xl' }}>
                        &copy; ブラタマチ.com All right reserved.
                    </Heading>
                </Flex>
            </Flex>
        </>
    );
});

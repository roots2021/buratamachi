/* eslint-disable react-hooks/exhaustive-deps */

import { Box, Flex, Heading, Link, useDisclosure } from '@chakra-ui/react';
import { memo, useCallback, VFC } from 'react';
import { useNavigate } from 'react-router-dom';
import { MenuIconButton } from '../../atoms/button/MenuIconButton';
import { MenuDrawer } from '../../molecules/MenuDrawer';

export const Header: VFC = memo(() => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const navigate = useNavigate();

    const onClickHome = useCallback(() => navigate('/'), []);

    return (
        <>
            <Box
                as="nav"
                bg="blue.700"
                color="white"
                mb={5}
                textAlign="center"
                padding={{ base: 3, md: 5 }}
                position="relative"
            >
                <Box as="a" _hover={{ cursor: 'pointer' }} onClick={onClickHome}>
                    <Heading as="h1" display="inline" fontSize={{ base: 'lg', md: 'xl' }}>
                        ブラタマチ.com
                    </Heading>
                </Box>
                <Flex
                    justify="space-between"
                    position="absolute"
                    top={{ base: '3', md: '5' }}
                    right={{ base: '5', md: '20' }}
                    w={{ base: '20%', md: '10%' }}
                >
                    <Link href="" isExternal>
                        <img src={'/images/facebook.png'} width={25} />
                    </Link>
                    <Link href="" isExternal>
                        <img src={'/images/twitter.png'} width={25} />
                    </Link>
                    <Link href="" isExternal>
                        <img src={'/images/instagram.png'} width={25} />
                    </Link>
                </Flex>
            </Box>
        </>
    );
});

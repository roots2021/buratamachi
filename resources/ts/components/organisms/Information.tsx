import { Text } from '@chakra-ui/react';
import { memo, VFC } from 'react';

type Props = {
    content: string;
};

export const Information: VFC<Props> = memo((props) => {
    const { content } = props;

    return (
        <Text textAlign="center" mb={5}>
            {content}
        </Text>
    );
});

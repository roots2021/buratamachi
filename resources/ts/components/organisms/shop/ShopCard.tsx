import { Box, Image, Stack, Text } from '@chakra-ui/react';
import { memo, VFC } from 'react';

type Props = {
    id: number;
    categoryName: string;
    introduction: string;
    shopName: string;
    shopStatus: number;
    thumbnailUrl: string;
    onClick: (id: number) => void;
};

export const ShopCard: VFC<Props> = memo((props) => {
    const { id, categoryName, shopName, introduction, thumbnailUrl, shopStatus, onClick } = props;

    return (
        <>
            <Box
                w={{ base: 'auto', lg: 300 }}
                bg="white"
                border="1px solid gray"
                shadow="lg"
                position="relative"
                p={8}
                m={{ base: 2, lg: 4 }}
                _hover={{ cursor: 'pointer', opacity: 0.8 }}
                onClick={() => onClick(id)}
            >
                <Stack spacing={4}>
                    <Image src={thumbnailUrl} alt={shopName} m="auto" />
                    <Text fontSize="lg" fontWeight="bold">
                        {categoryName}
                    </Text>
                    <Text fontSize="lg" fontWeight="bold">
                        {shopName}
                    </Text>
                    <Text fontSize="sm" color="gray">
                        {introduction}
                    </Text>
                </Stack>
                {shopStatus === 1 ? (
                    <Box
                        position="absolute"
                        left={0}
                        top={0}
                        width={0}
                        height={0}
                        borderStyle="solid"
                        borderWidth="60px 60px 0 0"
                        borderColor="#fffb00 transparent transparent transparent"
                    ></Box>
                ) : null}
                {shopStatus === 0 ? (
                    <Box
                        position="absolute"
                        left={0}
                        top={0}
                        width={0}
                        height={0}
                        borderStyle="solid"
                        borderWidth="60px 60px 0 0"
                        borderColor="#a0ff70 transparent transparent transparent"
                    ></Box>
                ) : null}
                {shopStatus === null ? <Box position="absolute" left={0} top={0}></Box> : null}
            </Box>
        </>
    );
});

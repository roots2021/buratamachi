import {
    Image,
    Link,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalOverlay,
    Stack,
    Text,
} from '@chakra-ui/react';
import { memo, VFC } from 'react';
import { Shop } from '../../../types/shop';

type Props = {
    shop: Shop | undefined;
    isOpen: boolean;
    onClose: () => void;
};

export const UserDetailModal: VFC<Props> = memo((props) => {
    const { shop, isOpen, onClose } = props;

    return (
        <Modal isOpen={isOpen} onClose={onClose} autoFocus={false} motionPreset="slideInBottom">
            <ModalOverlay>
                <ModalContent py={4}>
                    <ModalCloseButton />
                    <ModalBody mx={4}>
                        <Stack spacing={4}>
                            <Image src={shop?.thumbnail_url} alt={shop?.shop_name} m="auto" p={2} />
                            <Text>{shop?.category_name}</Text>
                            <Text>{shop?.shop_name}</Text>
                            <Text>{shop?.sub_introduction}</Text>
                            <Link href={shop?.shop_url} isExternal color="blue.500">
                                {shop?.shop_url}
                            </Link>
                        </Stack>
                    </ModalBody>
                </ModalContent>
            </ModalOverlay>
        </Modal>
    );
});

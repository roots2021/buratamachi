import { Box, Image, Stack, Text } from '@chakra-ui/react';
import { memo, VFC } from 'react';

type Props = {
    imageUrl: string;
};

export const MainVisual: VFC<Props> = memo((props) => {
    const { imageUrl } = props;

    return <Image src={imageUrl} alt="メインビジュアル" w="100%" m="auto" />;
});

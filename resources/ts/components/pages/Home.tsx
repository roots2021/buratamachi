import { memo, VFC } from 'react';
import { FooterLayout } from '../templates/FooterLayout';
import { HeaderLayout } from '../templates/HeaderLayout';
import { InformationLayout } from '../templates/Home/InformationLayout';
import { MainVisualLayout } from '../templates/Home/MainVisualLayout';
import { ShopCardLayout } from '../templates/Home/ShopCardLayout';

export const Home: VFC = memo(() => {
    return (
        <>
            <HeaderLayout></HeaderLayout>
            <InformationLayout></InformationLayout>
            <MainVisualLayout></MainVisualLayout>
            <ShopCardLayout></ShopCardLayout>
            <FooterLayout></FooterLayout>
        </>
    );
});

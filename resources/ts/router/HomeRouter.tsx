import { Home } from '../components/pages/Home';

export const homeRoutes = [
    {
        path: '/',
        children: <Home />,
    },
];

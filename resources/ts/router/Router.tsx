import { memo, VFC } from 'react';
import { Route, Routes } from 'react-router-dom';

import { homeRoutes } from '../router/HomeRouter';
import { Page404 } from '../components/pages/Page404';
import { HeaderLayout } from '../components/templates/HeaderLayout';
import { Home } from '../components/pages/Home';

export const Router: VFC = memo(() => {
    return (
        <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="/*" element={<Page404 />}></Route>
        </Routes>
    );
});

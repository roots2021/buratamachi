export type Information = {
    id: number;
    content: string;
};

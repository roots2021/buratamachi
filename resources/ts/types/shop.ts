export type Shop = {
    id: number;
    category_name: string;
    introduction: string;
    sub_introduction: string;
    shop_name: string;
    shop_rank: number;
    shop_status: number;
    shop_url: string;
    thumbnail_url: string;
};

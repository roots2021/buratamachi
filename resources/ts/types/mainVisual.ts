export type MainVisual = {
    id: number;
    image_url: string;
};

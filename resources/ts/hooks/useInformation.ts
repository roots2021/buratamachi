/* eslint-disable react-hooks/exhaustive-deps */

import axios from 'axios';
import { useCallback, useState } from 'react';

import { Information } from '../types/information';
import { useMessage } from './useMessage';

export const useInformation = () => {
    const { showMessage } = useMessage();
    const [loading, setLoading] = useState(false);
    const [information, setInformation] = useState<Array<Information>>([]);

    const getInformation = useCallback(() => {
        setLoading(true);
        axios
            .get<Array<Information>>('/api/information')
            .then((res) => setInformation(res.data))
            .catch(() => showMessage({ title: '取得に失敗しました', status: 'error' }))
            .finally(() => setLoading(false));
    }, []);

    return { getInformation, loading, information };
};

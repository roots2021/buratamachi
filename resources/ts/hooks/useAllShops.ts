/* eslint-disable react-hooks/exhaustive-deps */

import axios from 'axios';
import { useCallback, useState } from 'react';

import { Shop } from '../types/shop';
import { useMessage } from './useMessage';

export const useAllShops = () => {
    const { showMessage } = useMessage();
    const [loading, setLoading] = useState(false);
    const [shops, setShops] = useState<Array<Shop>>([]);

    const getShops = useCallback(() => {
        setLoading(true);
        axios
            .get<Array<Shop>>('/api/shop')
            .then((res) => setShops(res.data))
            .catch(() => showMessage({ title: '取得に失敗しました', status: 'error' }))
            .finally(() => setLoading(false));
    }, []);

    return { getShops, loading, shops };
};

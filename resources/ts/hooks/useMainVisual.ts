/* eslint-disable react-hooks/exhaustive-deps */

import axios from 'axios';
import { useCallback, useState } from 'react';

import { MainVisual } from '../types/mainVisual';
import { useMessage } from './useMessage';

export const useMainVisual = () => {
    const { showMessage } = useMessage();
    const [loading, setLoading] = useState(false);
    const [mainVisual, setMainVisual] = useState<Array<MainVisual>>([]);

    const getMainVisual = useCallback(() => {
        setLoading(true);
        axios
            .get<Array<MainVisual>>('/api/mainVisual')
            .then((res) => setMainVisual(res.data))
            .catch(() => showMessage({ title: '取得に失敗しました', status: 'error' }))
            .finally(() => setLoading(false));
    }, []);

    return { getMainVisual, loading, mainVisual };
};

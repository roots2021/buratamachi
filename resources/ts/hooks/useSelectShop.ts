import { useCallback, useState } from 'react';
import { Shop } from '../types/shop';

type Props = {
    id: number;
    shops: Array<Shop>;
    onOpen: () => void;
};

export const useSelectShop = () => {
    const [selectedShop, setSelectedShop] = useState<Shop>();

    const onSelectShop = useCallback((props: Props) => {
        const { id, shops, onOpen } = props;
        const targetShop = shops.find((shop) => shop.id === id);
        setSelectedShop(targetShop);
        onOpen();
    }, []);
    return { onSelectShop, selectedShop };
};

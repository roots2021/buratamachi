@extends('layouts.layout')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/top.css') }}">
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@endsection

@section('content')
<div>
    @if (isset($Information))
    <p class="text-center mt-3">{{ $Information->content }}</p>
    @endif

    @if (isset($MainVisual))
    <h1>
        <div class="top_main_visual" style="
            background-image: url({{ $MainVisual['image_url'] }});
            ">
        </div>
    </h1>
    @endif

    <ul class="shop_list">
        @foreach ($shops as $index => $shop)
        <li class="shop_list_item" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $index }}">
            <div class="shop_image_area" style="
                background-image: url({{ $shop['thumbnail_url'] }});
            ">
            </div>
            <p class="shop_name">{{ $shop->shop_name }}</p>
            <p class="shop_introduction">{!! nl2br($shop->introduction) !!}</p>
            <div class="@if ($shop->shop_status == 1) premium_badge @elseif ($shop->shop_status == 0) first_appearance_badge @endif"></div>
        </li>
        @endforeach
    </ul>

    @foreach ($shops as $index => $shop)
    <div class="modal fade" id="exampleModal{{ $index }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <ul class="shop_list">
                        <li class="shop_list_item" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $index }}">
                        <div class="shop_image_area" style="
                background-image: url({{ $shop['thumbnail_url'] }});
            "></div>
                            <p class="shop_name">{{ $shop->shop_name }}</p>
                            <p class="shop_introduction">{!! nl2br($shop->sub_introduction) !!}</p>
                            <a href="{{ $shop->shop_url }}" target="_blank">{{ $shop->shop_url }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection

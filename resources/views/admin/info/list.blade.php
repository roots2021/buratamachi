@extends('admin.layouts.app')

@section('css')
   <link rel="stylesheet" href="/admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
   <link rel="stylesheet" href="/admin/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
   <link rel="stylesheet" href="/admin/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css"><!-- =============== BOOTSTRAP STYLES ===============-->
@endsection

@section('sidebar_first_level_active', 'info')

@section('content')
<section class="section-container">
   <!-- Page content-->
   <div class="content-wrapper">
      <div class="content-heading">
         <div>お知らせ</div>
      </div>
      <div class="container-fluid">
         <!-- DATATABLE DEMO 1-->
         <div class="card">
            <div class="card-body">
               <table class="table table-striped my-4 w-100" id="datatable1">
                  <thead>
                     <tr>
                        <th data-priority="1">本文</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr class="gradeX">
                        @if ($Information)
                        <td>{{ $Information->content }}</td>
                        <td>
                        @endif
                           <a href="{{ route('admin.info.edit') }}" class="btn btn-outline btn-primary btn-sm">編集</a>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div><!-- DATATABLE DEMO 2-->
      </div>
   </div>
</section>
@endsection

   <!-- Datatables-->
@section('footer_js')
   <script src="vendor/datatables.net/js/jquery.dataTables.js"></script>
   <script src="vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
   <script src="vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
   <script src="vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.flash.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.html5.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.print.js"></script>
   <script src="vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
   <script src="vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
   <script src="vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   <script src="vendor/jszip/dist/jszip.js"></script>
   <script src="vendor/pdfmake/build/pdfmake.js"></script>
   <script src="vendor/pdfmake/build/vfs_fonts.js"></script>
@endsection

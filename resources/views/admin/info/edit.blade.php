@extends('admin.layouts.app')

@section('css')
	<link rel="stylesheet" href="/admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css">
	<link rel="stylesheet" href="/css/admin.css">
@endsection

@section('sidebar_first_level_active', 'info')

@section('content')
<section class="section-container">
	<div class="content-wrapper">
		<div class="content-heading">
			<div>お知らせ編集</div>
		</div>
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="card card-default">
						<div class="card-body">
							<form action="{{ route('admin.info.edit.post') }}" method="post">
								@csrf
								<div class="form-group">
									<label>お知らせタイトル <span class="attention">（必須）</span></label>
									<input class="form-control" type="text" name="content" value="@if ($Information ){{ $Information->content }} @endif">
								</div>
								<button class="btn mr-2 btn-sm btn-success" type="submit">保存</button><a href="{{ route('admin.info') }}" class="btn btn-outline btn-secondary btn-sm">戻る</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('footer_js')
	<script src="/admin/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="/admin/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
	<script src="/admin/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.print.js"></script>
	<script src="/admin/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
	<script src="/admin/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
	<script src="/admin/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script src="/admin/vendor/jszip/dist/jszip.js"></script>
	<script src="/admin/vendor/pdfmake/build/pdfmake.js"></script>
	<script src="/admin/vendor/pdfmake/build/vfs_fonts.js"></script>
@endsection

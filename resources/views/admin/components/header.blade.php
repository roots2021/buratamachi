<header class="topnavbar-wrapper">
	<nav class="navbar topnavbar">
		<div class="navbar-header">
			<a class="navbar-brand" href="#/" style="color: #fff;">
				<div class="brand-logo">ブラ田町</div>
			</a>
		</div>
		<ul class="navbar-nav flex-row">
			<li class="nav-item">
				<form action="{{ route('admin.logout') }}" method="post" class="nav-link">
					@csrf
					<button class="nav-link" href="#" data-toggle-state="offsidebar-open" data-no-persist="true" style="background-color:transparent;border:none;color:white;">
						<em class="icon-notebook"></em>
					</button>
				</form>
			</li>
		</ul>
	</nav>
</header>

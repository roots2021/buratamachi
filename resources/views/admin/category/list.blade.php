@extends('admin.layouts.app')

@section('css')
	<link rel="stylesheet" href="/admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css"><!-- =============== BOOTSTRAP STYLES ===============-->
	<link rel="stylesheet" href="/css/admin.css">
@endsection

@section('sidebar_first_level_active', 'category')

@section('content')
<section class="section-container">
	<div class="content-wrapper">
		<div class="content-heading">
			<div>カテゴリ一覧</div>
		</div>
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<a href="{{ route('admin.category.create') }}" class="btn btn-outline btn-success btn-sm">新規追加</a>
				</div>
				<div class="card-body">
					<table class="table table-striped my-4 w-100" id="datatable1">
						<thead>
							<tr>
								<th>カテゴリ名</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($Categories as $Category)
							<tr class="gradeX">
								<td>{{ $Category->category_name }}</td>
								<td>
									<a href="{{ route('admin.category.edit', $Category->id) }}" class="btn btn-outline btn-primary btn-sm">編集</a>
									<a href="{{ route('admin.category.delete', $Category->id) }}" class="btn btn-outline btn-danger btn-sm ml-3">削除</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('footer_js')
	<script src="/admin/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="/admin/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
	<script src="/admin/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.print.js"></script>
	<script src="/admin/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
	<script src="/admin/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
	<script src="/admin/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script src="/admin/vendor/jszip/dist/jszip.js"></script>
	<script src="/admin/vendor/pdfmake/build/pdfmake.js"></script>
	<script src="/admin/vendor/pdfmake/build/vfs_fonts.js"></script>
@endsection


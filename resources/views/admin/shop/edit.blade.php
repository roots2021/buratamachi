@extends('admin.layouts.app')

@section('css')
	<link rel="stylesheet" href="/admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css">
	<link rel="stylesheet" href="/css/admin.css">
@endsection

@section('sidebar_first_level_active', 'user')

@section('content')
<section class="section-container">
	<div class="content-wrapper">
		<div class="content-heading">
			<div>ショップ情報編集</div>
		</div>
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="card card-default">
						<div class="card-body">
							<form action="{{ route('admin.shop.edit.post', $Shop->id) }}" method="post" enctype="multipart/form-data">
								@csrf
								<input type="hidden" name="user_id" value="{{ $Shop->id }}">
								<div class="form-group">
									<label>ショップ名 <span class="attention">（必須）</span></label>
									<input class="form-control" type="text" name="shop_name" value="{{ $Shop->shop_name }}">
								</div>
                        <div class="form-group mt-4">
									<label>カテゴリ <span class="attention">（必須）</span></label><br>
									<select name="category_id">
										@foreach ($Categories as $Category)
										<option value="{{ $Category->id }}" @if ($Shop->category_id == $Category->id) selected @endif>{{ $Category->category_name }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group mt-4">
									<label>紹介文（カード） <span class="attention">（必須）</span></label>
									<textarea class="form-control" type="text" name="introduction" rows="10">{{ $Shop->introduction }}</textarea>
								</div>
								<div class="form-group mt-4">
									<label>紹介文（ポップアップ内） <span class="attention">（必須）</span></label>
									<textarea class="form-control" type="text" name="sub_introduction" rows="10">{{ $Shop->sub_introduction }}</textarea>
								</div>
								<div class="form-group mt-4">
									<label>ショップランク </label>
									<p>※プレミア枠の表示順<br>数字の小さい方から表示</p>
									<input class="form-control" type="text" name="shop_rank" value="{{ $Shop->shop_rank }}">
								</div>
								<div class="form-group mt-4">
									<label>ステータス <span class="attention">（必須）</span></label><br>
									<select name="shop_status">
										<option value="2" @if ($Shop->shop_status == 2) selected @endif>設定なし</option>
										<option value="0" @if ($Shop->shop_status == 0) selected @endif>初登場</option>
										<option value="1" @if ($Shop->shop_status == 1) selected @endif>プレミア</option>
									</select>
								</div>
								<div class="form-group mt-4">
									<label>ショップURL</label>
									<input class="form-control" type="text" name="shop_url" value="{{ $Shop->shop_url }}">
								</div>
								<div class="form-group mt-4">
									<label>サムネイル <span class="attention">（必須）</span></label>
									<input type="file" id="file" name="thumbnail_url" class="form-control" style="border: none;">
								</div>
								<button class="btn mr-2 btn-sm btn-success" type="submit">更新</button><a href="{{ route('admin.shop') }}" class="btn btn-outline btn-secondary btn-sm">戻る</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('footer_js')
	<script src="/admin/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="/admin/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
	<script src="/admin/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.print.js"></script>
	<script src="/admin/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
	<script src="/admin/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
	<script src="/admin/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script src="/admin/vendor/jszip/dist/jszip.js"></script>
	<script src="/admin/vendor/pdfmake/build/pdfmake.js"></script>
	<script src="/admin/vendor/pdfmake/build/vfs_fonts.js"></script>
@endsection

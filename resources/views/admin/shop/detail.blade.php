@extends('admin.layouts.app')

@section('css')
	<link rel="stylesheet" href="/admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
	<link rel="stylesheet" href="/admin/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css">
	<link rel="stylesheet" href="/css/admin.css">
@endsection

@section('sidebar_first_level_active', 'shop')

@section('content')
<section class="section-container">
	<div class="content-wrapper">
		<div class="content-heading">
			<div>ショップ情報詳細</div>
		</div>
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="card card-default">
						<table class="table">
							<tbody class="p-5">
								<tr>
									<th width="20%">ショップ名</th>
									<td>{{ $Shop->shop_name }}</>
								</tr>
								<tr>
									<th>カテゴリ</th>
									<td>{{ $Shop->category->category_name }}</td>
								</tr>
								<tr>
									<th>紹介文（カード）</th>
									<td>{!! nl2br($Shop->introduction) !!}</td>
								</tr>
								<tr>
									<th>紹介文（ポップアップ内）</th>
									<td>{!! nl2br($Shop->sub_introduction) !!}</td>
								</tr>
								<tr>
									<th>ショップランク</th>
									<td>{{ $Shop->shop_rank }}</td>
								</tr>
								<tr>
									<th>ステータス</th>
									<td>{{ $Shop->shop_status }}</td>
								</tr>
								<tr>
									<th>ショップURL</th>
									<td>{{ $Shop->shop_url }}</>
								</tr>
								<tr>
									<th>サムネイル</th>
									<td>
										<img src="{{ $Shop->thumbnail_url }}" alt="サムネイル" width="300px">
									</td>
								</tr>
							</tbody>
						</table>
						<p class="text-right mr-3"><a href="{{ route('admin.shop.edit', $Shop->id) }}" class="btn btn-outline btn-primary btn-sm mr-2">編集</a><a href="{{ route('admin.shop') }}" class="btn btn-outline btn-secondary btn-sm">戻る</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('footer_js')
	<script src="/admin/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="/admin/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
	<script src="/admin/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
	<script src="/admin/vendor/datatables.net-buttons/js/buttons.print.js"></script>
	<script src="/admin/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
	<script src="/admin/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
	<script src="/admin/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script src="/admin/vendor/jszip/dist/jszip.js"></script>
	<script src="/admin/vendor/pdfmake/build/pdfmake.js"></script>
	<script src="/admin/vendor/pdfmake/build/vfs_fonts.js"></script>
@endsection

@extends('admin.layouts.app')

@section('css')
   <link rel="stylesheet" href="/admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
   <link rel="stylesheet" href="/admin/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
   <link rel="stylesheet" href="/admin/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css">
@endsection

@section('sidebar_first_level_active', 'shop')

@section('content')
<section class="section-container">
   <div class="content-wrapper">
      <div class="content-heading">
         <div>ショップ一覧</div>
      </div>
      <div class="container-fluid">
         <div class="card">
            <div class="card-header">
					<a href="{{ route('admin.shop.create') }}" class="btn btn-outline btn-success btn-sm">新規追加</a>
				</div>
            <div class="card-body">
               <table class="table table-striped my-4 w-100" id="datatable1">
                  <thead>
                     <tr>
                        <th width="5%">ID</th>
                        <th width="15%">ショップ名</th>
                        <th width="30%">カード説明文</th>
                        <th width="30%">ポップアップ説明文</th>
                        <th width="20%">オプション</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($Shops as $Shop)
                     <tr class="gradeX">
                        <td>{{ $Shop->id }}</td>
                        <td>{{ $Shop->shop_name }}</td>
                        <td>{!! nl2br($Shop->introduction) !!}</td>
                        <td>{!! nl2br($Shop->sub_introduction) !!}</td>
                        <td>
                           <a href="{{ route('admin.shop.detail', $Shop->id) }}" class="btn btn-outline btn-info btn-sm">詳細</a>
                           <a href="{{ route('admin.shop.edit', $Shop->id) }}" class="btn btn-outline btn-primary btn-sm ml-3">編集</a>
                           <a href="{{ route('admin.shop.delete', $Shop->id) }}" class="btn btn-outline btn-danger btn-sm ml-3">削除</a>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection

@section('footer_js')
   <script src="vendor/datatables.net/js/jquery.dataTables.js"></script>
   <script src="vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
   <script src="vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
   <script src="vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.flash.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.html5.js"></script>
   <script src="vendor/datatables.net-buttons/js/buttons.print.js"></script>
   <script src="vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
   <script src="vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
   <script src="vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   <script src="vendor/jszip/dist/jszip.js"></script>
   <script src="vendor/pdfmake/build/pdfmake.js"></script>
   <script src="vendor/pdfmake/build/vfs_fonts.js"></script>
@endsection

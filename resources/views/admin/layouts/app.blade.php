<!DOCTYPE html>
<html lang="ja">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <title>ブラ田町 ADMIN</title>
   <link rel="icon" type="image/x-icon" href="favicon.ico">
    @yield('meta')

   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="/admin/vendor/@fortawesome/fontawesome-free/css/brands.css">
   <link rel="stylesheet" href="/admin/vendor/@fortawesome/fontawesome-free/css/regular.css">
   <link rel="stylesheet" href="/admin/vendor/@fortawesome/fontawesome-free/css/solid.css">
   <link rel="stylesheet" href="/admin/vendor/@fortawesome/fontawesome-free/css/fontawesome.css"><!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="/admin/vendor/simple-line-icons/css/simple-line-icons.css"><!-- ANIMATE.CSS-->
   <link rel="stylesheet" href="/admin/vendor/animate.css/animate.css"><!-- WHIRL (spinners)-->
   <link rel="stylesheet" href="/admin/vendor/whirl/dist/whirl.css"><!-- =============== PAGE VENDOR STYLES ===============-->
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="/admin/css/bootstrap.css" id="bscss"><!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="/admin/css/app.css" id="maincss">
   <link id="autoloaded-stylesheet" rel="stylesheet" href="/admin/css/theme-g.css">
   @yield('css')
</head>

<body>
   <div class="wrapper">
      <!-- top navbar-->
      @component('admin.components.header')
      @endcomponent

      @component('admin.components.sidebar')
        @slot('firstLevel')
          @yield('sidebar_first_level_active')
        @endslot
        {{-- @slot('secondLevel')
          @yield('sidebar_second_level_active')
        @endslot --}}
      @endcomponent

      @yield('content')

      <footer class="footer-container"><span>&copy; 2020 - Angle</span></footer>
   </div><!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="/admin/vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
   <script src="/admin/vendor/js-storage/js.storage.js"></script><!-- SCREENFULL-->
   <script src="/admin/vendor/screenfull/dist/screenfull.js"></script><!-- i18next-->
   <script src="/admin/vendor/i18next/i18next.js"></script>
   <script src="/admin/vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script>
   <script src="/admin/vendor/jquery/dist/jquery.js"></script>
   <script src="/admin/vendor/popper.js/dist/umd/popper.js"></script>
   <script src="/admin/vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- =============== APP SCRIPTS ===============-->
   <script src="/admin/js/app.js"></script>
   @yield('footer_js')
</body>

</html>

<?php

use App\Http\Controllers\TopController;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [TopController::class, 'index']);

Auth::routes();

Route::get('/admin/login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('/admin/login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'login'])->name('admin.login');
Route::post('/admin/logout', [App\Http\Controllers\Admin\Auth\LoginController::class, 'logout'])->name('admin.logout');

// ログイン認証後
    Route::middleware('auth:admin')->group(function () {
    Route::get('/admin/shop', [App\Http\Controllers\Admin\Shop\ShopController::class, 'index'])->name('admin.shop');
    Route::get('/admin/shop/create', [App\Http\Controllers\Admin\Shop\ShopController::class, 'create'])->name('admin.shop.create');
    Route::post('/admin/shop/create', [App\Http\Controllers\Admin\Shop\ShopController::class, 'store'])->name('admin.shop.store');
    Route::get('/admin/shop/{shop_id}', [App\Http\Controllers\Admin\Shop\ShopController::class, 'detail'])->name('admin.shop.detail');
    Route::get('/admin/shop/{shop_id}/edit', [App\Http\Controllers\Admin\Shop\ShopController::class, 'edit'])->name('admin.shop.edit');
    Route::post('/admin/shop/{shop_id}/edit', [App\Http\Controllers\Admin\Shop\ShopController::class, 'editPost'])->name('admin.shop.edit.post');
    Route::get('/admin/shop/{shop_id}/delete', [App\Http\Controllers\Admin\Shop\ShopController::class, 'delete'])->name('admin.shop.delete');

    Route::get('/admin/category', [App\Http\Controllers\Admin\Category\CategoryController::class, 'index'])->name('admin.category');
    Route::get('/admin/category/create', [App\Http\Controllers\Admin\Category\CategoryController::class, 'create'])->name('admin.category.create');
    Route::post('/admin/category/create', [App\Http\Controllers\Admin\Category\CategoryController::class, 'createPost'])->name('admin.category.create.post');
    Route::get('/admin/category/{category_id}/edit', [App\Http\Controllers\Admin\Category\CategoryController::class, 'edit'])->name('admin.category.edit');
    Route::post('/admin/category/{category_id}/edit', [App\Http\Controllers\Admin\Category\CategoryController::class, 'update'])->name('admin.category.update');
    Route::get('/admin/category/{category_id}/delete', [App\Http\Controllers\Admin\Category\CategoryController::class, 'delete'])->name('admin.category.delete');

    Route::get('/admin/info', [App\Http\Controllers\Admin\Info\InfoController::class, 'index'])->name('admin.info');
    Route::get('/admin/info/edit', [App\Http\Controllers\Admin\Info\InfoController::class, 'edit'])->name('admin.info.edit');
    Route::post('/admin/info/edit', [App\Http\Controllers\Admin\Info\InfoController::class, 'editPost'])->name('admin.info.edit.post');
    });

if (config('app.env') === 'production') {
    URL::forceScheme('https');
}

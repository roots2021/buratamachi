<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->text('introduction')->comment('店舗紹介文')->change();
            $table->text('sub_introduction')->comment('ポップアップ内に表示する店舗紹介文')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->string('introduction')->comment('店舗紹介文')->change();
            $table->string('sub_introduction')->comment('ポップアップ内に表示する店舗紹介文')->change();
        });
    }
};

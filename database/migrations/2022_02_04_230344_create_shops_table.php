<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->string('introduction')->comment('店舗紹介文');
            $table->string('sub_introduction')->comment('ポップアップ内に表示する店舗紹介文');
            $table->string('shop_name');
            $table->integer('shop_rank')->nullable()->comment('プレミア枠の表示順。数字が小さい方が先頭');
            $table->integer('shop_status')->nullable()->default(0)->comment('0:初登場 1:プレミア');
            $table->string('shop_url')->nullable();
            $table->string('thumbnail_url');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}

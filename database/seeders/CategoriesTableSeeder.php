<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'category_name' => 'カテゴリ1',
                'created_at' => '2022/02/06',
            ],
            [
                'category_name' => 'カテゴリ2',
                'created_at' => '2022/02/07',
            ],
            [
                'category_name' => 'カテゴリ3',
                'created_at' => '2022/02/08',
            ],
            [
                'category_name' => 'カテゴリ4',
                'created_at' => '2022/02/09',
            ],
        ]);
    }
}

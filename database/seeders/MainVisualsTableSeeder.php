<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MainVisualsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('main_visuals')->insert([
            [
                'image_url' => 'https://roots-tech.net/img/shop/hagi/top/main1.jpg',
                'created_at' => '2022/02/06',
            ],
            [
                'image_url' => 'https://roots-tech.net/img/shop/hagi/top/main2.jpg',
                'created_at' => '2022/02/07',
            ],
            [
                'image_url' => 'https://roots-tech.net/img/shop/hagi/top/main3.jpg',
                'created_at' => '2022/02/08',
            ],
            [
                'image_url' => 'https://roots-tech.net/img/shop/hagi/top/main4.jpg',
                'created_at' => '2022/02/09',
            ],
        ]);
    }
}

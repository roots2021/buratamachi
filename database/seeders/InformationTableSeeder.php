<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('information')->insert([
            [
                'content' => 'お知らせ1',
                'created_at' => '2022/02/06',
            ],
            [
                'content' => 'お知らせ2',
                'created_at' => '2022/02/07',
            ],
            [
                'content' => 'お知らせ3',
                'created_at' => '2022/02/08',
            ],
            [
                'content' => 'お知らせ4',
                'created_at' => '2022/02/09',
            ],
        ]);
    }
}

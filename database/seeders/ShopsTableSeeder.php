<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->insert([
            [
                'category_id' => 1,
                'introduction' => '自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1自己紹介1',
                'sub_introduction' => '自己紹介サブ1自己紹介サブ1自己紹介サブ1自己紹介サブ1自己紹介サブ1自己紹介サブ1自己紹介サブ1自己紹介サブ1自己紹介サブ1自己紹介サブ1',
                'shop_name' => '店舗1',
                'shop_rank' => 2,
                'shop_status' => 1,
                'shop_url' => 'https:google.com/',
                'thumbnail_url' => 'https://roots-tech.net/img/shop/hagi/top/main1.jpg',
                'created_at' => '2022/02/05',
            ],
            [
                'category_id' => 2,
                'introduction' => '自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2自己紹介2',
                'sub_introduction' => '自己紹介サブ2自己紹介サブ2自己紹介サブ2自己紹介サブ2自己紹介サブ2自己紹介サブ2自己紹介サブ2自己紹介サブ2自己紹介サブ2自己紹介サブ2',
                'shop_name' => '店舗2',
                'shop_rank' => 1,
                'shop_status' => 1,
                'shop_url' => 'https://www.yahoo.co.jp/',
                'thumbnail_url' => 'https://roots-tech.net/img/shop/hagi/top/main2.jpg',
                'created_at' => '2022/02/06',
            ],
            [
                'category_id' => 3,
                'introduction' => '自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3自己紹介3',
                'sub_introduction' => '自己紹介サブ3自己紹介サブ3自己紹介サブ3自己紹介サブ3自己紹介サブ3自己紹介サブ3自己紹介サブ3自己紹介サブ3自己紹介サブ3自己紹介サブ3',
                'shop_name' => '店舗3',
                'shop_rank' => 0,
                'shop_status' => 0,
                'shop_url' => 'https://www.rakuten.co.jp/',
                'thumbnail_url' => 'https://roots-tech.net/img/shop/hagi/top/main3.jpg',
                'created_at' => '2022/02/07',
            ],
            [
                'category_id' => 4,
                'introduction' => '自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4自己紹介4',
                'sub_introduction' => '自己紹介サブ4自己紹介サブ4自己紹介サブ4自己紹介サブ4自己紹介サブ4自己紹介サブ4自己紹介サブ4自己紹介サブ4自己紹介サブ4自己紹介サブ4',
                'shop_name' => '店舗4',
                'shop_rank' => 0,
                'shop_status' => 0,
                'shop_url' => 'https://roots-tech.net/',
                'thumbnail_url' => 'https://roots-tech.net/img/shop/hagi/top/main4.jpg',
                'created_at' => '2022/02/08',
            ],
        ]);
    }
}

const mix = require('laravel-mix');

mix.sass('resources/sass/top.scss', 'public/css').sass('resources/sass/common.scss', 'public/css');
